#ifndef ZOOM_IN_HPP_5ABE8977_DAAB_4036_9626_02B788B3675E_
#define ZOOM_IN_HPP_5ABE8977_DAAB_4036_9626_02B788B3675E_
#include "MouseEvent.hpp"

class ZoomIn : public MouseEvent
{
public:
  ZoomIn(int key);
  virtual ~ZoomIn();
  static MouseEvent *create(int key);
  void pressed(int value);
  static string getKeyword();

private:
  static string KEYWORD;
};


#endif  // ZOOM_IN_HPP_5ABE8977_DAAB_4036_9626_02B788B3675E_
