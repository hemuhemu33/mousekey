#ifndef GO_UPPER_RIGHT_EVENT_HPP_2C4B4380_A5FE_41D8_993B_1676DF216702_
#define GO_UPPER_RIGHT_EVENT_HPP_2C4B4380_A5FE_41D8_993B_1676DF216702_


#include "MouseEvent.hpp"
class GoUpperRightEvent : public MouseEvent
{
public:
  GoUpperRightEvent(int key);
  virtual ~GoUpperRightEvent();
  static MouseEvent *create(int key);
  void pressed(int value);
  static string getKeyword();

private:
  static string KEYWORD;
};

#endif  // GO_UPPER_RIGHT_EVENT_HPP_2C4B4380_A5FE_41D8_993B_1676DF216702_
