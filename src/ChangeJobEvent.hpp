#ifndef CHANGE_JOB_EVENT_HPP_6DB0035D_46EE_4547_8816_D0088293D7B9_
#define CHANGE_JOB_EVENT_HPP_6DB0035D_46EE_4547_8816_D0088293D7B9_

#include "MouseEvent.hpp"
#include "HyperKeyEvent.hpp"

class ChangeJobEvent : public MouseEvent
{
  
public:
  ChangeJobEvent(int key);
  virtual ~ChangeJobEvent();
  static MouseEvent *create(int key);
  void pressed(int value);
  static string getKeyword();
private:
  static string KEYWORD;
};




#endif  // CHANGE_JOB_EVENT_HPP_6DB0035D_46EE_4547_8816_D0088293D7B9_
