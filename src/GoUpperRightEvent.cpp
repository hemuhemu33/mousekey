#include "GoUpperRightEvent.hpp"

string GoUpperRightEvent::KEYWORD = "GOUPPERRIGHTEVENT";
GoUpperRightEvent::~GoUpperRightEvent(){
  
}

GoUpperRightEvent::GoUpperRightEvent(int key):MouseEvent(key){
  setKeyword(KEYWORD);
}

string GoUpperRightEvent::getKeyword(){
  return KEYWORD;
}

MouseEvent *GoUpperRightEvent::create(int key){
  MouseEvent *gue = new GoUpperRightEvent(key);
  return gue;
}

void GoUpperRightEvent::pressed(int value){
  if ((value == 1 || value == 2)) {
    if ((writeKeyEvent(REL_X,1*MouseEvent::getSpeed(),EV_REL,Uinput::getfd()) == false)||(writeKeyEvent(REL_Y,-1*MouseEvent::getSpeed(),EV_REL,Uinput::getfd()) == false)) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
    if (writeKeyEvent(EV_SYN, 0, SYN_REPORT, Uinput::getfd()) == false) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
  }

}
