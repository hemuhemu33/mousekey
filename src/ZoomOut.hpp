#ifndef ZOOM_OUT_HPP_5CC5AFD8_21AE_47E6_9C1C_AFF558A0DD44_
#define ZOOM_OUT_HPP_5CC5AFD8_21AE_47E6_9C1C_AFF558A0DD44_

#include "MouseEvent.hpp"

class ZoomOut : public MouseEvent
{
public:
  ZoomOut(int key);
  virtual ~ZoomOut();
  static MouseEvent *create(int key);
  void pressed(int value);
  static string getKeyword();

private:
  static string KEYWORD;
};


#endif  // ZOOM_OUT_HPP_5CC5AFD8_21AE_47E6_9C1C_AFF558A0DD44_
