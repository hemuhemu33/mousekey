#ifndef GO_BOTTOM_RIGHT_EVENT_HPP_DEEB418B_9677_4FAD_A4F6_E7E880FF6C6C_
#define GO_BOTTOM_RIGHT_EVENT_HPP_DEEB418B_9677_4FAD_A4F6_E7E880FF6C6C_


#include "MouseEvent.hpp"
class GoBottomRightEvent : public MouseEvent
{
public:
  GoBottomRightEvent(int key);
  virtual ~GoBottomRightEvent();
  static MouseEvent *create(int key);
  void pressed(int value);
  static string getKeyword();

private:
  static string KEYWORD;
};


#endif  // GO_BOTTOM_RIGHT_EVENT_HPP_DEEB418B_9677_4FAD_A4F6_E7E880FF6C6C_
