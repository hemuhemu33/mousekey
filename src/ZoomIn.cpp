#include "ZoomIn.hpp"

string ZoomIn::KEYWORD = "ZOOMINEVENT";
ZoomIn::~ZoomIn(){
  
}

ZoomIn::ZoomIn(int key):MouseEvent(key){
  setKeyword(KEYWORD);
}

string ZoomIn::getKeyword(){
  return KEYWORD;
}

MouseEvent *ZoomIn::create(int key){
  MouseEvent *gue = new ZoomIn(key);
  return gue;
}

void ZoomIn::pressed(int value){

  if (writeKeyEvent(KEY_LEFTCTRL,value,EV_KEY,Uinput::getfd()) == false) {
    std::cerr << "Can't write key" << "\n";
    exit(1);
  }
  if (writeKeyEvent(EV_SYN, 0, SYN_REPORT, Uinput::getfd()) == false) {
    std::cerr << "Can't write key" << "\n";
    exit(1);
  }
  if (value >= 1) {
    if (writeKeyEvent(REL_WHEEL,1,EV_REL,Uinput::getfd()) == false) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
    if (writeKeyEvent(EV_SYN, 0, SYN_REPORT, Uinput::getfd()) == false) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
  }else {}
  



}

