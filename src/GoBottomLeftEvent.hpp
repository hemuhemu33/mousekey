#ifndef GO_BOTTOM_LEFT_EVENT_HPP_FE1A35DA_ABB9_4AED_A437_6A4B37C69821_
#define GO_BOTTOM_LEFT_EVENT_HPP_FE1A35DA_ABB9_4AED_A437_6A4B37C69821_


#include "MouseEvent.hpp"
class GoBottomLeftEvent : public MouseEvent
{
public:
  GoBottomLeftEvent(int key);
  virtual ~GoBottomLeftEvent();
  static MouseEvent *create(int key);
  void pressed(int value);
  static string getKeyword();

private:
  static string KEYWORD;
};


 
#endif  // GO_BOTTOM_LEFT_EVENT_HPP_FE1A35DA_ABB9_4AED_A437_6A4B37C69821_
