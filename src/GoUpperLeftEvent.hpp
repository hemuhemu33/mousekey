#ifndef GO_UPPER_LEFT_EVENT_HPP_995FAA70_1483_4743_935D_F978B30C179A_
#define GO_UPPER_LEFT_EVENT_HPP_995FAA70_1483_4743_935D_F978B30C179A_

#include "MouseEvent.hpp"
class GoUpperLeftEvent : public MouseEvent
{
public:
  GoUpperLeftEvent(int key);
  virtual ~GoUpperLeftEvent();
  static MouseEvent *create(int key);
  void pressed(int value);
  static string getKeyword();

private:
  static string KEYWORD;
};

#endif  // GO_UPPER_LEFT_EVENT_HPP_995FAA70_1483_4743_935D_F978B30C179A_
 
