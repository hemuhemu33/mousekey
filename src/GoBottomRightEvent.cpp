#include "GoBottomRightEvent.hpp"

string GoBottomRightEvent::KEYWORD = "GOBOTTOMRIGHTEVENT";
GoBottomRightEvent::~GoBottomRightEvent(){
  
}

GoBottomRightEvent::GoBottomRightEvent(int key):MouseEvent(key){
  setKeyword(KEYWORD);
}

string GoBottomRightEvent::getKeyword(){
  return KEYWORD;
}

MouseEvent *GoBottomRightEvent::create(int key){
  MouseEvent *gue = new GoBottomRightEvent(key);
  return gue;
}

void GoBottomRightEvent::pressed(int value){
  if ((value == 1 || value == 2)) {
    if ((writeKeyEvent(REL_X,1*MouseEvent::getSpeed(),EV_REL,Uinput::getfd()) == false)||(writeKeyEvent(REL_Y,1*MouseEvent::getSpeed(),EV_REL,Uinput::getfd()) == false)) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
    if (writeKeyEvent(EV_SYN, 0, SYN_REPORT, Uinput::getfd()) == false) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
  }

}
