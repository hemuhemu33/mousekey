#include "ZoomOut.hpp"

string ZoomOut::KEYWORD = "ZOOMOUTEVENT";
ZoomOut::~ZoomOut(){
  
}

ZoomOut::ZoomOut(int key):MouseEvent(key){
  setKeyword(KEYWORD);
}

string ZoomOut::getKeyword(){
  return KEYWORD;
}

MouseEvent *ZoomOut::create(int key){
  MouseEvent *gue = new ZoomOut(key);
  return gue;
}

void ZoomOut::pressed(int value){

  if (writeKeyEvent(KEY_LEFTCTRL,value,EV_KEY,Uinput::getfd()) == false) {
    std::cerr << "Can't write key" << "\n";
    exit(1);
  }

  if (writeKeyEvent(EV_SYN, 0, SYN_REPORT, Uinput::getfd()) == false) {
    std::cerr << "Can't write key" << "\n";
    exit(1);
  }
  if (value >= 1) {
    if (writeKeyEvent(REL_WHEEL,-1,EV_REL,Uinput::getfd()) == false) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
    if (writeKeyEvent(EV_SYN, 0, SYN_REPORT, Uinput::getfd()) == false) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
  }else {}
  



}

