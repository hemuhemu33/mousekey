#include "ChangeJobEvent.hpp"

string ChangeJobEvent::KEYWORD = "CHANGEJOBEVENT";
ChangeJobEvent::~ChangeJobEvent(){
  
}

ChangeJobEvent::ChangeJobEvent(int key):MouseEvent(key){
  setKeyword(KEYWORD);
}

string ChangeJobEvent::getKeyword(){
  return KEYWORD;
}

MouseEvent *ChangeJobEvent::create(int key){
  MouseEvent *gue = new ChangeJobEvent(key);
  return gue;
}

void ChangeJobEvent::pressed(int value){
  //押した瞬間(value = 1)なら、Alt=1,Tab=1を出力
  //押している間(value = 2)なら、Alt=2,Tab=0
  //離した時(value = 0)ならAltを離す。Alt=0,Tab=0
  if (value == 1) {
    if ((writeKeyEvent(KEY_LEFTALT,value,EV_KEY,Uinput::getfd()) == false) || (writeKeyEvent(KEY_TAB,value,EV_KEY,Uinput::getfd()) == false)) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }    
  }else if (value == 2) {
    if ((writeKeyEvent(KEY_LEFTALT,value,EV_KEY,Uinput::getfd()) == false) || (writeKeyEvent(KEY_TAB,0,EV_KEY,Uinput::getfd()) == false)) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
  }else if(value == 0){
    if ((writeKeyEvent(KEY_LEFTALT,0,EV_KEY,Uinput::getfd()) == false) || (writeKeyEvent(KEY_TAB,0,EV_KEY,Uinput::getfd()) == false)) {
      std::cerr << "Can't write key" << "\n";
      exit(1);
    }
  }else {
    
  }

  
  if (writeKeyEvent(EV_SYN, 0, SYN_REPORT, Uinput::getfd()) == false) {
    return;
  }

}

